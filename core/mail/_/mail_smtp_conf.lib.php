<?php

/**
 * Mail SMTP Config
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

class mail_smtp_conf extends standardObject {
	
	var $SMTPDebug = 0;
	var $SMTPAuth = 'login';
	var $SMTPSecure = 'ssl';
	var $Host = 'server';
	var $Port = 465;
	var $Username = 'username';
	var $Password = 'password';

}