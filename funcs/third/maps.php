<?php

/**
 * Gyural > 3rd Funcs > Maps
 *
 * @version 1.10
 * @author Andrea Rufo <a.rufo@mandarinoadv.com>
 */

function maps__getJson($ind){

	$ind = str_replace(" ", "+", $ind);
	$json = json_decode(file_get_contents('http://maps.google.com/maps/api/geocode/json?address=%22'.$ind.'%22&components=country:it|&sensor=false&language=it'));

	return $json;

}

// ritorna un array con latitudine e longitudine dell'indirizzo passato
function maps__GetLatLng($ind) {

	$json = maps__getJson($ind);

	$latlng = array(
		'lat' => $json->results[0]->geometry->location->lat,
		'lng' => $json->results[0]->geometry->location->lng
	);

	return $latlng;

}

// ritorna una stringa con l'indirizzo formattato correttamente
function maps__GetAddress($ind) {

	$json = maps__getJson($ind);

	// echo '<pre>'.print_r($json->results[0]->formatted_address, 1).'</pre>';

	return $json->results[0]->formatted_address;

}

// stampa una mappa del punto indicato e delle dimensioni assegnate
function maps__CreateMap($lat, $lng, $w = '100%', $h = '600px', $js = true) {

	if($js)
		echo '<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>';

	?>
	<div id="map" style="width: <?=$w?>; height: <?=$h?>"></div>
	<script type="text/javascript">

		var myLatlng = new google.maps.LatLng(<?=$lat?>,<?=$lng?>);

		var myOptions = {
			zoom: 14,
			center: myLatlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		var map = new google.maps.Map(document.getElementById("map"), myOptions);

		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			// title:"Posizione!"
		});
	</script>
	<?php
}

//	http://maps.google.com/maps/api/geocode/json?address=%22.$what.%22&components=country:it|&sensor=false&language=it
//	https://developers.google.com/maps/documentation/javascript/markers#add
//	GetLatLong($indirizzo) e CreaMappa(lat, long, width, height)

?>