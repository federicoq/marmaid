# marmaid

## Tabelle dal Database

```
Sql  CREATE TABLE `media` (
  `media_id` int(11) NOT NULL AUTO_INCREMENT,
  `instance_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `content` TEXT NOT NULL,
  `checksum` varchar(255) NOT NULL,
  `media_type_id` int(11) NOT NULL,
  `creationTime` int(11) NOT NULL,
  `updateTime` int(11) NOT NULL,
  PRIMARY KEY (`media_id`)
);

CREATE TABLE `tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `creationTime` int(11) NOT NULL,
  `updateTime` int(11) NOT NULL,
  `isBoard` int(1) NOT NULL,
  `isBoardPublic` int(1) NOT NULL,
  PRIMARY KEY (`tag_id`)
);

CREATE TABLE `media_tags` (
  `media_tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`media_tag_id`)
);

CREATE TABLE `tags_users` (
  `tags_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `creationTime` int(11) NOT NULL,
  `updateTime` int(11) NOT NULL,
  `role` int(3) NOT NULL,
  PRIMARY KEY (`tags_user_id`)
);
```