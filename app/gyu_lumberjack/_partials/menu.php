<? if(is_array($app_data)): ?>
<ul>
	<? foreach($app_data as $main): ?>
	<li>
		<? echo $main["name"]; ?>
		<? if(is_array($main["items"])): ?>
			<ul>
				<? foreach($main["items"] as $item): ?>
				<li><a href="<? echo $item[1]; ?>"><? echo $item[0]; ?></a></li>
				<? endforeach; ?>
			</ul>
		<? endif; ?>
	</li>
	<? endforeach; ?>
</ul>
<? endif; ?>