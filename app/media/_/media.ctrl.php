<?php

class mediaCtrl extends AppController {


	function create($args = null) {

		if(!$args || !is_array($args)) {
			$this->error = 1;
			$this->errors[] = 'Argouments not given.';
		}

		if(!$args['user_id'] && !logged()) {
			$this->error = 1;
			$this->errors[] = 'No user given. And not logged in.';
		} else if(logged()) {
			$args['user_id'] = Me()->id;
		}

		// Check if the user exists.
		$user = LoadClass('users', 1)->get($args['user_id']);
		if(!$user) {
			$this->error = 1;
			$this->errors[] = 'User not found.';
		}

		// Great, now we'll have to `understand` what sort of content we're trying to post.
		$this->MediaManagerName = 'media/manager/' . str_replace('_', '/', $args['type']);

		if(!is_object($this->MediaManager = LoadClass($this->MediaManagerName, 1))) {

			$this->error = 1;
			$this->errors[] = 'Impossible to manage this sort of data.';

		}


	}

}