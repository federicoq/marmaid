<?php

/**
 * Gyural > Funcs > Security
 * Security functions to validate or invalidate application execution
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

/**
 * Check if the user can execute the current context
 * 
 * @param string $m The lower lever to execute the app
 * @param string $ifBad the location to load if not allowed
 */
function Right($m, $ifBad = null) {
	// In $m there's the lower level for execute the app
	if($m === "*") {
		// Star mean that you'll have at least to be logged...
		// For a security reason, if the $m is * we'll set the application level to 99 and the executor to 100.
		$m = 99;
		if(Logged())
			$accessLevel = 100;
		else
			$accessLevel = -1;
	} else {
		// In that case, if the user is not logged, his access level will be set at 0
		if(!isset($_SESSION["login"]))
			$accessLevel = 0;
		else {
			// Here's the real control on applications.
			$debug = debug_backtrace();
			$pagina = $debug[0]["file"];
			list($pagina) = explode("/", str_replace(array(application, '.php'), "", $pagina));
			
			$res = FetchObject(Database()->query("SELECT * FROM `".tablesPrefix."rights` WHERE `group_id` = '".$_SESSION["login"]->group."' AND `application` = '".$pagina."' AND `right` >= $m LIMIT 1"));
			
			if(!is_object($res))
				$accessLevel = -1;
			else
				$accessLevel = $res->right;
		}
	}
	
	if($m > $accessLevel) {
		if($ifBad == null)
			header('Location: '.uriLogin);
			// No way. The user cannot execute this application.
		else
			header("Location: " . $ifBad);
	}
	
}

/**
 * Check if I (Me()) can execute $what with $level
 * 
 * @param  string $what
 * @param  integer $level
 * @return boolean
 */
function i_can($what, $level) {
	$res = FetchObject(Database()->query("SELECT * FROM `".tablesPrefix."rights` WHERE `group_id` = '".Me()->getAttr('group')."' AND `application` = '".$what."' AND `right` >= $level LIMIT 1"));
	if(is_object($res))
		return true;
	else
		return false;
}

/**
 * Check if the user is logged
 * 
 * @return boolean
 */
function Logged() {
	if(@is_object($_SESSION["login"]) && $_SESSION["login"]->active == 1)
		return true;
	else
		return false;
}

/**
 * If $obj = false, it return $_SESSION["login"] array. Otherwise set $obj as $_SESSION["login"]
 * 
 * @param mixed $obj
 */
function Me($obj = false) {
	if($obj != false)
		$_SESSION["login"] = $obj;
	if(Logged())
		return $_SESSION["login"];
}
